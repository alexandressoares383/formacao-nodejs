const express = require('express');
const app = express();

app.get('/', function(req, res) {
    res.send("Bem vindo.");
});
app.get('/blog/:artigo?', function (req, res) {

    var artigo = req.params.artigo;

    if(artigo) {
        res.send("<h2>Artigo: " + artigo + "</h2>");
    } else {
        res.send("Bem vindo ao meu blog.");
    }

});
app.get('/canal/youtube', function (req, res) {
    var canal = req.query["canal"];

    if(canal) {
        res.send(canal);
    } else {
        res.send("Nenhum canal fornecido!")
    }
});
app.get('/ola/:nome', function (req, res) {
    var nome = req.params.nome; 
    res.send("<h1>Oi " + nome + "</h1>");
});

app.listen(8181, function(erro) {
    if(erro) {
        console.log("Ocorreu um erro!");
    } else {
        console.log("Servidor iniciado com sucesso!");
    }
});
